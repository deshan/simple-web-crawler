import crawler
import MailExtracter

# seedUrl = start point url
seedUrl='http://www.google.lk/'

#depth = how deep web page should be crawled
depth=0;

crawl=True
exMails=True

#crawl
if crawl:
    cr= crawler.Crawler(seedUrl,depth)
    cr.start()
     path=cr.getBasePath()
    
#extract mails
 if exMails:
     me=MailExtracter.MailExtracter(path)
     me.start()
     me.printMailList()
