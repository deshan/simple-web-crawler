# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* A simple web crawler used to extract  web page contents .
* Version : 1.0
 

### How do I get set up? ###

* Open project in a preferred Python IDE (Eclipse was used to develop this project) 
* Edit settings.py

    1.   BASE_SAVE_PATH: Relative path to save crawled web pages.
    2.   STATUS_CODES: Valid response codes to be crawled.   
    3.   VALID_MIME_TYPES: Valid response Mime types to be crawled.



* Run Main.py


### Authors  ###

* T.D Ruhunage
* H.A.M Sandaruwan