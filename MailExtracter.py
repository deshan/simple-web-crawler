import re
import files
import settings
from os.path import join


mailRgx =[ re.compile(r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")]
formattedMailRgx =[re.compile(r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*\s?\(at\)\s?(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"),
                   re.compile(r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*\sat\s(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\sdot\s)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")]
formatRgx={r'\(':r'',r'at':r'@',r'\)':r'',r'\s':r'',r'dot':r'.'}

class MailExtracter:
    
    fileSavePath=''
    basePath=''
    mailList=set()
    
    def __init__(self,path):
        print '############################################################################'
        print 'Harvesting mail addresses started.'
        self.basePath=path
        self.fileSavePath=join(self.basePath,settings.DEFAULT_FILE_FOLDER)
            
    def start(self):
        fileList=files.getFiles(self.fileSavePath)
        if fileList is not None:
            self.harvestEmails(fileList)
        print str(len(self.mailList))+' mail addresses found'
        if len(self.mailList) > 0:
            self.saveMailsToFile()
        
    def harvestEmails(self,files = []):
        
        if files != None:
            for f in files: 
                for line in open(f,'r'): 
                    for r in mailRgx:                         
                        self.mailList.update(r.findall(line))
                    for r in formattedMailRgx:
                        found=r.findall(line)                                           
                        self.mailList.update(self.formatMail(found)) 
         
    def formatMail(self,mailList):
        formattedList=[]
        for mail in mailList:
            for key in formatRgx.keys():
                pattern=re.compile(key,re.IGNORECASE)
                mail=re.sub(pattern,str(formatRgx[key]),mail)
            print mail
            if self.isValidMail(mail):              
                formattedList.append(mail)
        return formattedList
    
    def isValidMail(self,mail):
        for r in mailRgx:
            if r.match(mail) is not None:
                return True   
        return False
          
    def saveMailsToFile(self):
        files.saveListToFile(self.mailList,'mails.txt',self.basePath) 
         
    def printMailList(self):
        for m in self.mailList:
            print m
                           
    