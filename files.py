from  os.path import isdir,join,isfile,exists
from os import listdir,makedirs
import pickle

def getFiles(path):
    onlyFiles=''
    if isdir(path):
        onlyFiles=[]
        onlyFiles = [ join(path,f) for f in listdir(path) if isfile(join(path,f)) ]        
    return onlyFiles
      
def saveFile(data,filename,path):
    if isdir(path):
        savePath=join(path,filename)
        newfile = open(savePath, 'w+')
        newfile.write(data)
        newfile.close()
        
    else:
        createDirectory(path)   
        saveFile(data,filename,path)
        
def createDirectory(directory):
    if not exists(directory):
        makedirs(directory)       
        
def saveListToFile(mailList,filename,path):
    if isdir(path):
        savePath=join(path,filename)
        newfile = open(savePath, 'w+')
        for line in mailList:
            newfile.write(str(line)+'\n')
        newfile.close()
    else:
        createDirectory(path)   
        saveListToFile(mailList,filename,path)        