import urllib2
import re 
import settings 
import httplib
import files
import uuid
from urlparse import urlparse
from os.path import join

linkRgx =re.compile(r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', re.IGNORECASE)
mimeTypeRgx=re.compile(r'^[A-Z,a-z]+[A-Z,a-z,1-9,]*/[A-Z,a-z,1-9]+')

class Crawler: 
    
    fileSavePath=''
    basePath=''
    seed=''
    depth=0

    
    def __init__(self,seed,depth):
        print '############################################################################'
        print 'Crawling started.'
        print 'Files are saved at path :'+self.basePath
        parseResult=urlparse(seed)
        self.basePath=join(settings.BASE_SAVE_PATH,parseResult.netloc)
        self.fileSavePath=join(self.basePath,settings.DEFAULT_FILE_FOLDER)
        print self.fileSavePath
        self.seed=seed
        self.depth=depth
        
    def getBasePath(self):
        return self.basePath
    
    def getFileSaveth(self):
        return self.fileSavePath
         
    def getResponseHeader(self,url):
        urlParse=urlparse(url)
        conn = httplib.HTTPConnection(urlParse.netloc)
        conn.request("HEAD",urlParse.path)
        res = conn.getresponse()
        return res
    
    def downloadFile(self,url):
        res = self.getResponse(url)
        data=res.read()
        filename = uuid.uuid4()
        files.saveFile(data, str(filename), self.fileSavePath)
        return data
       
    def getResponse(self,url):
        res = urllib2.urlopen(url)
        return res
    
    def harvestLinks(self,text):
        links = set()
        if text is not  None:
            found=linkRgx.findall(text)
            links.update(found)      
        return links
    
    def isValidLink(self,url):
        res=self.getResponseHeader(url)
        if res is not None and str(res.status) in settings.STATUS_CODES:
            head=res.getheader("Content-Type")
            if head is not None:
                mimeType=mimeTypeRgx.match(head).group(0)
                print '\tContent type:'+mimeType 
                if mimeType  in settings.VALID_MIME_TYPES:
                    return True      
        return False
              
    def start(self):
        self.crawl([self.seed], self.depth)
        
    def crawl(self,url,depth):
        tocrawl=set()
        tocrawl.update(url)
        newLinks=set()
        count=0
        
        print '=================================================================================='
        print 'Depth :'+str(depth)
        print '=================================================================================='
        while tocrawl:
            page=str(tocrawl.pop())
            count=count+1
            print '['+str(count)+']'+'------------------------------------------'
            try:       
                print 'Checking Link:'+page+'...'
                if self.isValidLink(page):
                    print '\tCrawling...'
                    print '\tDownloading..'
                    content=self.downloadFile(page)
                    print '\tDownloading completed'
                    print '\tHarvesting links...'
                    foundLinks=self.harvestLinks(content);
                    newLinks.update(foundLinks)
                    print '\tHarvesting links completed'
                    print '\tCrawling completed-'+str(len(foundLinks))+' links found'
                    
                else:
                    print 'Skipped Error Url:'+page
                    continue
            except :
                print 'Error Crawling link:'+page
            
              
        if(depth>0):
            self.crawl(newLinks, depth=depth-1)  
        else:
            return     
            
            
            
            
            


    

